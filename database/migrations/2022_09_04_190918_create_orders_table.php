<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('name', 55);
            $table->string('phone', 55);
            $table->string('metro', 255);
            $table->string('address', 255);

            $table->tinyInteger('number_workers');
            $table->tinyInteger('wage');
            $table->tinyInteger('minimum_wage');
            $table->tinyInteger('working_hours');

            $table->text('comment');

            $table->date('date');
            $table->time('time');

            $table->enum('status', ['in-search', 'in-work', 'completed', 'canceled'])->default('in-search');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
